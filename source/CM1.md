CM1 stat
=======

mes stat

````{prf:example}
**Une matrice (4,2) : 4 lignes, 2 colonnes**

```{math}
\begin{equation*}\mathbf{M} = \left(\begin{array}{cc} 0 & 1  \\ \colorbox{yellow} {-6} & 2  \\ 1 & \colorbox{green} 8  \\-5 & 7 \end{array} \right)\end{equation*}
```

````
