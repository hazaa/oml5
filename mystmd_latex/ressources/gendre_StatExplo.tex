\documentclass[10pt]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[version=4]{mhchem}
\usepackage{stmaryrd}
\usepackage{bbold}
\usepackage{graphicx}
\usepackage[export]{adjustbox}
\graphicspath{ {./images/} }

\title{Chapitre 3 }

\author{}
\date{}


\begin{document}
\maketitle
\section*{Observations de deux variables couplées}
\subsection*{3.1 Introduction}
La dernière section du chapitre précédent présente un exemple de comparaison entre deux jeux de données relatifs à des variables $x$ et $y$. Lorsque ces jeux de données sont issus d'une même expérience, nous parlerons d'observations couplées. Dans ce cas, le statisticien peut naturellement se poser nombre de questions sur leurs liens potentiels : existe-t-il une relation entre $x$ et $y$ ? Si oui, de quelle nature? Si non, comment le justifier? ...

Dans ce chapitre, nous considérons donc deux jeux de même taille $n>0$ d'observations couplées $x_{1}, \ldots, x_{n}$ et $y_{1}, \ldots, y_{n}$ relatifs à des variables quantitatives $x$ et $y$ respectivement. Ces observations seront pondérées par les poids $p_{1}, \ldots, p_{n}>0$ normalisés.

\subsection*{3.2 Covariance et corrélation linéaire}
Un première façon d'établir un lien entre les variables $x$ et $y$ consiste à regarder si les observations ont tendance à varier dans le même sens.

Définition 3.1. La covariance entre les observations de $x$ et celles de $y$ est définie par

$$
\operatorname{Cov}(x, y)=\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)
$$

Le signe de la covariance a une signification importante. En effet, la covariance $\operatorname{Cov}(x, y)$ aura tendance à être positive si, pour de nombreux $i \in\{1, \ldots, n\}$, nous avons $x_{i} \geqslant \bar{x}$ et $y_{i} \geqslant \bar{y}$ ou bien si nous avons $x_{i} \leqslant \bar{x}$ et $y_{i} \leqslant \bar{y}$. Autrement dit, nous aurons

\begin{itemize}
  \item $\operatorname{Cov}(x, y)>0$ si les variables $x$ et $y$ ont tendance à varier dans le même sens,
  \item $\operatorname{Cov}(x, y)<0$ si les variables $x$ et $y$ ont tendance à varier en sens inverse.
\end{itemize}

Lorsque la covariance est proche de 0 , il n'est pas possible de l'interpréter directement.

Exemple Si $x$ est la température extérieure et si $y$ est le volume de crème glacée acheté, les observations de $x$ et de $y$ auront tendance à varier dans le même sens (plus il fait chaud, plus il y a de glaces consommées) et la covariance sera positive. Si $x$ est la température extérieure\\
et si $y$ est la consommation de gaz pour le chauffage, les observations de $x$ et de $y$ évolueront en sens inverse (plus il fait chaud, moins nous chauffons les maisons) et la covariance sera négative.


\subsection*{3.3 Régression linéaire}
Dans toute cette section, nous supposerons que les poids $p_{1}, \ldots, p_{n}$ sont uniformes, i.e. $p_{1}=\cdots=p_{n}=1 / n$.

Lorsque nous cherchons à établir une relation entre deux variables quantitatives $x$ et $y$, une première approche simple consiste à regarder si il existe une relation affine (i.e. de la forme $y=a x+b$ avec $a, b \in \mathbb{R}$ ) entre elles. Bien entendu, en pratique, il est presque toujours impossible d'établir une telle relation de façon exacte entre les observations de $x$ et celles de $y$. Cependant, nous pouvons chercher la droite qui explique "au mieux" $y$ par rapport à $x$.

Cette procédure s'appelle la régression linéaire et elle se formalise comme ce qui suit. Nous cherchons deux nombres réels $a$ et $b$ tels que l'erreur commise en expliquant les observations $y_{i}$ par $a x_{i}+b, i \in\{1, \ldots, n\}$, soit la plus petite possible au sens des moindres carrés. Autrement dit, nous cherchons $a, b \in \mathbb{R}$ tels que l'erreur moyenne


\begin{equation*}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-\left(a x_{i}+b\right)\right)^{2} \tag{3.1}
\end{equation*}


soit minimale (voir Figure 3.1). Les valeurs de $a$ et de $b$ telles que cette erreur soit minimale donnent l'équation $y=a x+b$ de la droite de régression.

\begin{center}
\includegraphics[max width=\textwidth]{images/2024_07_22_504b856845de313b4dabg-5}
\end{center}

Figure 3.1 - Erreurs (en rouge) commises entre les observations et la droite $y=0.5-x$.

Théorème 3.2. Si les variables $x$ et $y$ sont centrées et réduites alors l'erreur (3.1) est minimale pour $a=\operatorname{Cov}(x, y)$ et $b=0$. Dans ce cas, l'équation de la droite de régression est donc

$$
y=\operatorname{Cov}(x, y) \times x
$$

Démonstration. Nous commençons par développer l'erreur (3.1) en utilisant le fait que $\bar{x}=$\\
$\bar{y}=0$,

$$
\begin{aligned}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-\left(a x_{i}+b\right)\right)^{2} & =\frac{1}{n} \sum_{i=1}^{n}\left(\left(y_{i}-a x_{i}\right)-b\right)^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}-\frac{2 b}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)+\frac{1}{n} \sum_{i=1}^{n} b^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}-2 b(\bar{y}-a \bar{x})+b^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}+b^{2}
\end{aligned}
$$

La quantité que nous cherchons à minimiser est la somme de deux termes positifs, l'un dépendant de $a$ et l'autre de $b$. Nous obtenons donc directement que $b=0$. Pour déterminer $a$, nous continuons à développer cette quantité en utilisant que $\overline{x^{2}}=\operatorname{Var}(x)=1$ et $\overline{y^{2}}=\operatorname{Var}(y)=1$,

$$
\begin{aligned}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2} & =\frac{1}{n} \sum_{i=1}^{n} y_{i}^{2}-\frac{2 a}{n} \sum_{i=1}^{n} y_{i} x_{i}+\frac{a^{2}}{n} \sum_{i=1}^{n} x_{i}^{2} \\
& =\overline{y^{2}}-2 a \overline{x y}+a^{2} \overline{x^{2}} \\
& =a^{2}-2 \overline{x y} \times a+1
\end{aligned}
$$

Nous minimisons donc ce polynôme du second degré en $a=\overline{x y}=\operatorname{Cov}(x, y)$.

Le résultat du théorème 3.2 se généralise à des variables quantitatives $x$ et $y$ qui ne sont plus supposées être centrées réduites de la façon suivante.

Corollaire 3.1. Dans le cas général, l'erreur (3.1) est minimale pour $a=\operatorname{Cov}(x, y) / \operatorname{Var}(x)$ et $b=\bar{y}-a \bar{x}$ et l'équation de la droite de régression est donnée par

$$
y=\frac{\operatorname{Cov}(x, y)}{\operatorname{Var}(x)} x+\left[\bar{y}-\frac{\operatorname{Cov}(x, y)}{\operatorname{Var}(x)} \bar{x}\right]
$$

Démonstration. Considérons les variables quantitatives $x^{\prime}$ et $y^{\prime}$ obtenues en centrant et en réduisant $x$ et $y$ respectivement,

$$
x^{\prime}=\frac{x-\bar{x}}{\sigma_{x}} \quad \text { et } \quad y^{\prime}=\frac{y-\bar{y}}{\sigma_{y}} .
$$

Pour ces variables, le Théorème 3.2 nous donne l'équation de la droite de régression,

$$
\begin{aligned}
y^{\prime}=\operatorname{Cov}\left(x^{\prime}, y^{\prime}\right) \times x^{\prime} & \Longleftrightarrow \frac{y-\bar{y}}{\sigma_{y}}=\operatorname{Cov}\left(\frac{x-\bar{x}}{\sigma_{x}}, \frac{y-\bar{y}}{\sigma_{y}}\right) \times \frac{x-\bar{x}}{\sigma_{x}} \\
& \Longleftrightarrow y-\bar{y}=\frac{\operatorname{Cov}(x, y)}{\sigma_{x} \sigma_{y}} \times \frac{\sigma_{y}}{\sigma_{x}} \times(x-\bar{x}) \\
& \Longleftrightarrow y=\underbrace{\frac{\operatorname{Cov}(x, y)}{\sigma_{x}^{2}}}_{=a} x+\underbrace{\bar{y}-\frac{\operatorname{Cov}(x, y)}{\sigma_{x}^{2}} \bar{x}}_{=b}
\end{aligned}
$$

Nous retrouvons dans ce résultat le fait que si $\operatorname{Cov}(x, y)>0$, alors les variables varient dans le même sens et inversement (voir Figures 3.2 et 3.3).

Exercice 3.2. Le Corollaire 3.1 donne la droite de régression de y par rapport à $x$ d'équation $y=a x+b$. Si $a \neq 0$, nous pouvons en déduire que $x=a^{\prime} y+b^{\prime}$ avec $a^{\prime}=1 / a$ et $b^{\prime}=-b / a$. Calculer l'équation de la droite de régression de $x$ sur $y$ (i.e. trouver $\bar{a}, \bar{b} \in \mathbb{R}$ avec $x=\bar{a} y+\bar{b}$ ) et comparer-la avec $x=a^{\prime} y+b^{\prime}$. Conclure que les droites de régression de $y$ sur $x$ et de $x$ sur y ne sont pas les mêmes.

\begin{center}
\includegraphics[max width=\textwidth]{images/2024_07_22_504b856845de313b4dabg-7}
\end{center}

Figure 3.2 - Tracés des observations $\left(x_{i}, y_{i}\right)$ et de la droite de régression (en rouge) associée. A gauche, $\rho(x, y)=0.9855$ et à droite, $\rho(x, y)=-0.9863$.

\begin{center}
\includegraphics[max width=\textwidth]{images/2024_07_22_504b856845de313b4dabg-7(1)}
\end{center}

Figure 3.3 - Tracés des observations $\left(x_{i}, y_{i}\right)$ et de la droite de régression (en rouge) associée. Dans les deux cas, $\rho(x, y)=0$ et nous voyons qu'il n'est pas possible d'interpréter cette valeur car les variables $x$ et $y$ peuvent être indépendantes (à gauche) ou liées (à droite).


\end{document}
