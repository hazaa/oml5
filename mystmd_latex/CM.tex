\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[french]{babel}
\usepackage{tcolorbox}
\usepackage{pgfplots}
% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[letterpaper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{booktabs}
\newtheorem{thm}{Théorème}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{ex}{Exemple}[section]
\newtheorem{proposition}{Proposition}[section]


\title{CM OML5}
\author{A.~Hazan}

\begin{document}
\maketitle


\tableofcontents

%-----------------------------------------------------------
\section{Introduction}

Ce cours est une adaptation de \cite{Whilhelm22}, \cite{Gendre12} et de Wikistat.

\subsection{PPN}

Ressource R5.04, Outils Mathématiques et Logiciels, Apprentissages visés \cite{PPN2022} :
\begin{itemize}
\item Calculer des probabilités et statistiques (moyenne, écart type, variance) ;
\item Déterminer les variables aléatoires discrètes et continues, lois usuelles ;
\item Utiliser les calculs des probabilités pour une prise de décision ;
\item Déterminer une estimations ponctuelle et par intervalles de confiance : Appliquer à la maîtrise statistiques des procédés, à la maintenance des systèmes de production et la fiabilité.
\end{itemize}

\subsection{Planning}

\begin{tabular}{c|c|c|c}
Nature & Thème  & Commentaire &  FA  \\
\hline
CM1&Stat. decriptives &&6/09\\
TD1&Stat. decriptives &&09/09\\
TD2&Stat. decriptives &&13/09\\
TD3&Stat. decriptives&&23/09\\
TD4&Stat. decriptives &&23/09\\
TD5&Révisions&&30/09\\
DS& &&04/10\\
\end{tabular}

%-----------------------------------------------------------
\section{Pourquoi faire des statistiques en GEII}

Les technicien·e·s/ingénieur·e·s/scientifiques s'appuient sur les \textbf{mesures}.

Toute mesure d'un phénomène physique est bruitée:
\begin{itemize}
\item du fait du phénomène lui-même, qui comporte une part d'aléatoire.
\item du fait de la mesure.
\end{itemize}

La Fig.\ref{fig.mesure.bruit} en donne un exemple. Les statistiques et les probabilités
sont les outils mathématiques appropriés pour traiter le bruit, et le hasard en général.

\begin{figure}
\centering
\includegraphics[width=0.3\textwidth]{PDFSVG/pink_noise_time_domain.svg.png}
\caption{Mesure bruitée. \href{https://commons.wikimedia.org/wiki/File:Pink_noise_time_domain.svg}{wikipedia}}
\label{fig.mesure.bruit}
\end{figure}
    
%-----------------------------------------------------------
\section{Types de données}

Nous allons définir un certain nombre de concepts et de termes reliés à l'analyse de données.
Cela nous donnera un premier aperçu de la notion d'aléa. En particulier, on abordera les concepts clés suivants:
\begin{itemize}
\item population, échantillon, individu, variable;
\item catégories de variables: qualitatives (ordinales/nominales), quantitatives (discrètes/continues).
\end{itemize}

%-----------
\subsection{Population et échantillon}

\begin{definition} ~\\
    \begin{itemize}
    \item Une \textbf{population} est l'ensemble sur lequel porte une étude statistique. 
    \item Un \textbf{échantillon} est un sous-ensemble (en général connu) de la population.
    \end{itemize}
\end{definition} 

Typiquement, on va récolter des données sur un certain nombre \textbf{d'individus} de la \textbf{population}. On va ensuite analyser \textbf{l'échantillon} des individus dont on a obtenu les données.  

On veut étudier une (ou plusieurs) caractéristique que possède chaque individu, une \textbf{variable statistique}. Par exemple, le poids de chaque individu dans la population.

\begin{ex} 
Alice (en GEA) veut organiser une vente de gâteaux à l'IUT. Elle voudrait savoir si elle doit plutôt préparer des gâteaux au chocolat ou au citron. Pour cela, elle demande à dix amis s'ils préfèrent le chocolat ou le citron.
\begin{itemize}
\item population: les étudiant·e·s et employé·e·s de GEA;
\item échantillon: les ami·e·s d'Alice
\item individu: un·e ami·e d'Alice
\item variable / donnée: préférence entre chocolat et citron
\end{itemize}
\end{ex}
%-----------
\subsection{Types de variables}

On différencie deux types de variables:

\begin{itemize}
\item Les variables \textbf{qualitatives} qui expriment une caractéristique:
    \begin{itemize}
    \item ordinales (= hiérarchie): « un peu, beaucoup, passionément, à la folie »;
    \item nominales: « femme, homme »;  
   \end{itemize}
   
\item Les variables \textbf{quantitatives} qui ont une valeur numérique:
    \begin{itemize}
    \item discrètes: seulement certaines valeurs sont possibles (souvent entières)
    \item continues: n'importe quelle valeur est possible dans un intervalle.
    \end{itemize}
\end{itemize}

\begin{ex} 
Pour les exemples suivants, pensez-vous que ce sont des variables qualitatives ou quantitatives discrètes ou quantitatives continues.  
    \begin{itemize}
    \item vote pendant une élection;
    \item poids;
    \item lancer d'un dé;
    \item quantité de chocolat consommé en une année;
    \item être gaucher ou droitier;
    \end{itemize}
\end{ex}

\begin{ex} 
Pour les exemples suivants, pensez-vous que ce sont des variables qualitatives ou quantitatives discrètes ou quantitatives continues.  
    \begin{itemize}
    \item vote pendant une élection: qualitatif
    \item poids: quantitatif continu
    \item lancer d'un dé: quantitatif discret
    \item quantité de chocolat consommé en une année: quantitatif continu
    \item être gaucher ou droitier: à première vue, qualitatif mais on pourrait aussi quantifier le degré de préférence pour la main droite entre $-1$ et $+1$ ce qui rendrait cette variable quantitative continue.
    \end{itemize}
\end{ex}

%-----------------------------------------------------------
\section{Représentations graphiques}

\begin{definition} 
Un \textbf{diagramme en barres} pour une variable qualitative ou quantitative discrète représente 
le nombre d'individus de chaque modalité dans un échantillon par des barres de hauteur proportionnelle.
\end{definition} 


\begin{ex} 

Le CHU de Créteil veut connaitre la fréquence des différents
groupes sanguins en Ile de France, pour prévoir des stocks appropriés.
Pour cela, on a mesuré le groupe sanguin de 25 donneurs.

	\begin{tabular}{ccc}
		\hline
		Classe & Effectif & Fréquence \\
		\hline
		$A$ &5 & 5/25 = 0.2		\\
		$B$ & 8 & 8/25 = 0.32	\\
		$O$ & 8 & 8/25 = 0.32	\\
		$AB$ & 4 & 4/25 = 0.16	\\
		\hline
		Total & 25 & 25/25=1	\\
		\hline
	\end{tabular}


    \begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{PDFSVG/bar_plot}
    \caption{Diagramme en barres. La hauteur de chaque colonne correspond à la proportion dans l’échantillon du groupe sanguin.}
    \end{figure}
    
\end{ex} 

\begin{definition} 
Un \textbf{histogramme} pour une variable quantitative continue $x$ représente 
le nombre d'individus pour lesquels la valeur $x$ se trouve dans des intervalles 
$]x_0;x_1], ]x_1;x_2],\ldots$
\end{definition} 


\begin{ex} 
Le poids de 92 étudiants d’une école américaine a été relevé dans une unité anglaise particulière, le pound (symbole"lb").

Les données observées figurent dans le tableau \ref{tab:poids}.

\begin{table}
\centering
{\tiny 
	\begin{tabular}{ccccccccccc}
	\hline
	\multicolumn{11}{l}{Hommes}\\
	\hline
		140 & 145 & 160 & 190 & 155 & 165 & 150 & 190 & 195 & 138 & 160 \\
		155 & 153 & 145 & 170 & 175 & 175 & 170 & 180 & 135 & 170 & 157 \\
		130 & 185 & 190 & 155 & 170 & 155 & 215 & 150 & 145 & 155 & 155 \\
		150 & 155 & 150 & 180 & 160 & 135 & 160 & 130 & 155 & 150 & 148 \\
		155 & 150 & 140 & 180 & 190 & 145 & 150 & 164 & 140 & 142 & 136 \\
		123 & 155 &&&&&&&&&\\
		&&&&&&&&&&\\
		\multicolumn{11}{l}{Femmes}\\
		\hline
		140 & 120 & 130 & 138 & 121 & 125 & 116 & 145 & 150 & 112 & 125 \\
		130 & 120 & 130 & 131 & 120 & 118 & 125 & 135 & 125 & 118 & 122 \\
		115 & 102 & 115 & 150 & 110 & 116 & 108 & 95  & 125 & 133 & 110 \\
		150 & 108 &&&&&&&&&\\
	\hline
	\end{tabular}
	}
\caption{\label{tab:poids}Poids de 92 étudiants d’une école américaine}
\end{table}

On peut transformer une variable continue en variable discrète.

On choisit une division en intervalles, et on compte le nombre d’observations correspondant à chaque intervalle. On obtient alors une \textbf{table de fréquences}:

{\small
\begin{tabular}{rlrrrr}
  \hline
 & Classe & Centre & Effectif & Fréquence & Fréquence cumulée\\ 
  \hline
   & ]80 - 100] &  90 &   1 & 0.01  & 0.01 \\ 
   & ]100 - 120] & 110 &  15 & 0.16 & 0.17 \\ 
   & ]120 - 140] & 130 &  25 & 0.27 & 0.44 \\ 
   & ]140 - 160] & 150 &  33 & 0.36 & 0.80\\ 
   & ]160 - 180] & 170 &  11 & 0.12 & 0.92\\ 
   & ]180 - 200] & 190 &   6 & 0.07 & 0.99\\ 
   & ]200 - 220] & 210 &   1 & 0.01 & 1.0\\ 
   \hline
  Somme &  &  &  92 & 1.00  & \\ 
   \hline
\end{tabular}
}

On peut alors construire un \textbf{histogramme}, comme représenté Fig. \ref{fig:histogramme}.

    \begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{PDFSVG/histogrammes}
    \caption{\label{fig:histogramme}Histogramme des masses (en pounds, lb). (haut) décompte ; (bas) fréquences normalisées; (gauche) intervalles de largeur 10; (droite) intervalles de largeur 20.}
    \end{figure}

On peut varier le nombre d’intervalles afin de changer le niveau de précision de l’histogramme.

\end{ex} 

%TRANSITION: poids cumulés GEndre 2.3 ???

\begin{definition} 
La \textbf{fonction de répartition} peut être vue comme un histogramme des cumulé 
pour lequel il n’y aurait qu’une unique observation dans chaque intervalle.

Pour un échantillon $x_1,\ldots, x_N$ mesuré, c'est une fonction $F_x$ constante par morceaux et croissante de $0$ à $1$ définie 
pour tout $t \in \mathbb{R}$ par: 

\begin{equation}
F_x(t)  =\frac{1}{N} \sum_{i ~\textrm{tel que} ~x_i\leq t}  1
\end{equation}
\end{definition} 

\begin{ex}
Nous avons les $N = 5$ observations suivantes:
\begin{equation}
x_1 = 3, ~x_2= -1, ~x_3=4, ~x_4=3, ~x_5 = 0
\end{equation}
La version ordonnée de ces observation est donc:
\begin{equation}
x_{(1)} = x_2 = -1 , ~x_{(2)} = x_5 = 0, ~x_{(3)}= x_1 = 3, ~x_{(4)} = x_4 = 3 , x_{(5)} = x_3 = 4 .
\end{equation}
La Fig. \ref{fig.fonction.repartition} représente la fonction de répartition associée.


    \begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{PDFSVG/fonction_repartition}
    \caption{\label{fig.fonction.repartition}Fonction de répartition de $x_1 = 3, ~x_2= -1, ~x_3=4, ~x_4=3, ~x_5 = 0$}
    \end{figure}
\end{ex}



%-----------------------------------------------------------
\section{Statistique descriptive à une variable}

On va donner des outils pour résumer ou décrire rapidement une variable quantitative.

\begin{itemize}
\item notions de centre: \textbf{moyenne}, médiane;
\item notions de dispersion: \textbf{variance} et \textbf{écart-type}, étendue interquartile;
\item quantiles;
\end{itemize}


%---------------    
\subsection{Caractéristiques d’une variable}
    
%position (médiane, moyenne)
%dispersion (étendue, var/std, quartiles, IQ)
%Pour des \textbf{variables quantitatives}, on s'intéresse à:

\begin{itemize}
\item la \textbf{tendance centrale}: le milieu des données, une valeur typique, e. g.,  la moyenne et la médiane;
\item la \textbf{dispersion}: à quel point les données fluctuent autour du centre. e. g., l'écart-type et l'étendue ou écart inter-quartile;
\item la \textbf{symétrie} ou \textbf{asymétrie} autour du centre;
\item le cas échéant, le nombre de « bosses » ou \textbf{modes};
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.3\textwidth]{PDFSVG/distributions}
\caption{\label{fig:distributions}Distributions: tendance centrale, dispersion,symétrie. (haut) 
Même distribution mais centres différents, Même centre, dispersions différentes
(bas) Centres et dispersions différents, Distribution symétrique et asymétrique.   }
\end{figure}
    
%---------------    
\subsection{Mesures de tendance centrale d'une variable quantitative}
        
\begin{definition} 
La \textbf{moyenne} (arithmétique) d'une suite de valeurs $x_1, \dots, x_n$ est:
\begin{gather*}
 \bar{x} = \frac{x_1 + \dots + x_n}{n} = \frac{1}{n} \left( \sum_{i=1}^n x_i \right) 
\end{gather*}
\end{definition} 
        
\begin{ex}     % see make_figures.py: tab_tension()
Dans le Tab.\ref{tab.tension} on mesure 10 fois la tension aux bornes d'une résistance,
on constate la présence d'un bruit. On calcule la moyenne arithmetique.
\input{PDFSVG/tab_tension.tex}
\end{ex}
        
        
\begin{definition} 
Statistique d'ordre. 

On a mesuré une même variable $n$ fois, et on dispose des observations suivantes
\begin{gather*}
 x_1, x_2, \dots, x_n.
\end{gather*}
On peut trier cette série dans l'ordre croissant des valeurs. Les valeurs ordonnées sont notées avec un indice entre parenthèses:
\begin{gather*} x_{(1)} \leq x_{(2)} \leq \dots \leq x_{(n)} \end{gather*}
Ainsi, $ x_{(1)} $ est le minimum,  $ x_{(n)}$ le maximum et $ x_{(i)}$ est la $i$ème plus grande valeur observée. On dit aussi parfois que $ x_{(i)}$ est la $i$ème \textbf{statistique d'ordre}.
\end{definition} 
        

\begin{definition} 
La médiane, la valeur qui « coupe les données en deux »: 50\% sont plus petites, et 50\% sont plus grandes.
La médiane est la valeur qui est à la position centrale:
\begin{gather*}
 med = x_{ ( \lceil n/2 \rceil ) } 
\end{gather*}
Il existe aussi une définition symétrique qui est parfois utilisée:
\begin{gather*}
    med(x) =
    \left\{
    \begin{array}{ll}
    x_{ \left( \frac{n+1}{2} \right) }         & \text{si $n$ est impair}, \\
    & \\
    \frac{1}{2}  \Big( x_{\left( \frac{n}{2} \right)}+x_{\left( \frac{n}{2} + 1 \right) } \Big)                                                                 & \text{si n est pair}. \\
    \end{array}
    \right.
\end{gather*}
\end{definition}

\begin{ex}     % see make_figures.py: tab_tension_mediane()
Dans le Tab.\ref{tab.tension.mediane} on mesure 11 fois la tension aux bornes d'une résistance,
on constate la présence d'un bruit. On calcule la médiane.
\input{PDFSVG/tab_tension_mediane.tex}
\end{ex}

%!!!TODO: freq cumules, quartiles

%!!!TODO: ROBUSTESSE DE LA MEDIANE!!!

La médiane coupe les données en deux parties égales. On peut
généraliser cette idée en séparant l’échantillon en parties inégales.


%---------------    
\subsection{Mesure de dispersion d'une variable quantitative}

\begin{definition} 
\textbf{La variance} (empirique) de l'échantillon $ x_1, x_2, \dots, x_n$ est :
\begin{gather*}
 s^2 = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{x})^2  
\end{gather*}  
\end{definition} 

La variance est donc la moyenne des carrés des écarts $x_1 - \bar{x}, \ldots, x_n - \bar{x}$. Cette quantité mesure la dispersion des observations autour de x. De plus, elle est
toujours positive.

% TODO: par la formule de koenig, E[(X-\hat{X})^2]=E[X^2] -  E[\hat{X}^2]

\begin{definition} 
\textbf{L'écart-type} (empirique) de l'échantillon $ x_1, x_2, \dots, x_n$ est la racine carrée de la variance :
\begin{gather*}
 s = \sqrt{ \frac{1}{n} \sum_{i=1}^n (x_i - \bar{x})^2 } 
\end{gather*}  
On peut également le noter $\sigma_x$.
\end{definition} 


On a vu deux mesures de tendances centrales et deux mesures de dispersion: pourquoi? 
La plupart du temps, les valeurs données par ces différentes notions vont être proches.
    
La médiane et l'écart interquartile sont moins sensibles aux valeurs aberrantes. 
C'est une bonne chose car ces valeurs résultent souvent d'une erreur de mesure ou
d'un problème dans la collecte des données. 
On dit de ces mesures qu'elles sont \textbf{robustes}.
    
On va se concentrer sur la \textbf{moyenne} et \textbf{l'écart-type} (et la \textbf{variance}) par la suite.


% EXEMPLES: loi normale, ....


%---
\subsection{Variables centrées et réduites}
%SOURCE: wikistat

Soit $X$ est une variable quantitative de moyenne $\bar{x}$ et d’écart–type $\sigma_X$ ,

\begin{definition} 
On appelle \textbf{variable centrée} associée à $X$ la variable:
\begin{eqnarray}
X-\bar{x}
\end{eqnarray}

\end{definition} 

\begin{thm} 
Une variable centrée est de moyenne nulle et d’écart–type  $\sigma_X$.
\end{thm}

\begin{definition} 
On appelle variable \textbf{centrée et réduite} (ou tout simplement
variable réduite) associée à $X$ la variable:
\begin{eqnarray}
\frac{X-\bar{x}}{\sigma_X}
\end{eqnarray}

\end{definition} 

\begin{thm} 
Une variable centrée-réduite est de moyenne nulle et d’écart–type égal à $1$.
\end{thm}





%-----------------------------------------------------------    
\section{Statistique descriptive à 2 variables}

La dernière section du chapitre précédent présente un exemple de comparaison entre deux jeux 
de données relatifs à des variables $x$ et $y$. Lorsque ces jeux de données sont issus d'une 
même expérience, nous parlerons d'observations couplées. Dans ce cas, le statisticien
 peut naturellement se poser nombre de questions sur leurs liens potentiels : existe-t-il une
  relation entre $x$ et $y$ ? Si oui, de quelle nature? Si non, comment le justifier? ...

Dans ce chapitre, nous considérons donc deux jeux de même taille $n>0$ d'observations 
couplées $x_{1}, \ldots, x_{n}$ et $y_{1}, \ldots, y_{n}$ relatifs à des variables quantitatives
 $x$ et $y$ respectivement. 


%---------------    
\subsection{Nuage de points}

%source wikisat "st-l-des-bi.pdf" "Statistique descriptive bidimensionnelle"

Il s’agit d’un graphique très commode pour représenter les observations 
simultanées de deux variables quantitatives. Il consiste à considérer deux axes 
perpendiculaires, l’axe horizontal représentant la variable X et l’axe vertical
la variable Y , puis à représenter chaque individu observé par les coordonnées
des valeurs observées. L’ensemble de ces points donne en général une idée
assez bonne de la variation conjointe des deux variables et est appelé nuage. On
notera qu’on rencontre parfois la terminologie de diagramme de dispersion,
traduction plus fidèle de l’anglais scatter-plot.

\begin{ex}~\\
 %U=RI régression https://webetab.ac-bordeaux.fr/Pedagogie/Physique/Physico/Electro/e07ohm.htm
% http://pcsi2.carnot.dijon.free.fr/dotclear/public/Polycopies/Mesure%20et%20incertitudes.pdf
\begin{center}
        \begin{tikzpicture}
        \begin{axis}[xlabel=X, ylabel=Y]
            \addplot[only marks] coordinates {(0,0) (1,0.7) (2,0) (3,1) (4,1) (5,2) (1,0.5) (3,0.78)}; 
        \end{axis}
    \end{tikzpicture} 
\end{center}
\end{ex}    
%---------------    
\subsection{Covariance et corrélation}

%pour corr:  idem
%(whilhelm: trop complexe, modèle gaussien)    

%Exemple: (elle varient ds meme sens ou pas; cf gendre p.17)

Un première façon d'établir un lien entre les variables $x$ et $y$ consiste à regarder si les observations ont tendance à varier dans le même sens.

\begin{definition} 
La covariance entre les observations de $x$ et celles de $y$ est définie par
$$
\operatorname{Cov}(x, y)=\frac{1}{n}\sum_{i=1}^{n} \left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)
$$
\end{definition} 

Le signe de la covariance a une signification importante. En effet, la covariance $\operatorname{Cov}(x, y)$ aura tendance à être positive si, pour de nombreux $i \in\{1, \ldots, n\}$, nous avons $x_{i} \geq \bar{x}$ et $y_{i} \geq \bar{y}$ ou bien si nous avons $x_{i} \leq \bar{x}$ et $y_{i} \leq \bar{y}$. Autrement dit, nous aurons

\begin{itemize}
  \item $\operatorname{Cov}(x, y)>0$ si les variables $x$ et $y$ ont tendance à varier dans le même sens,
  \item $\operatorname{Cov}(x, y)<0$ si les variables $x$ et $y$ ont tendance à varier en sens inverse.
\end{itemize}

Lorsque la covariance est proche de 0 , il n'est pas possible de l'interpréter directement.

\begin{ex}
Si $x$ est la température extérieure et si $y$ est le volume de crème glacée acheté, 
les observations de $x$ et de $y$ auront tendance à varier dans le même sens
(plus il fait chaud, plus il y a de glaces consommées) et la covariance sera positive. 

Si $x$ est la température extérieure et si $y$ est la consommation de gaz pour le chauffage, 
les observations de $x$ et de $y$ évolueront en sens inverse
(plus il fait chaud, moins nous chauffons les maisons) et la covariance sera négative.
\end{ex}


En dehors de son signe, la valeur de la covariance $\operatorname{Cov}(x, y)$ ne donne pas beaucoup plus d'informations car elle est dépendante de l'échelle des variables $x$ et $y$. Pour contourner cela, il faut normaliser les observations et considérer la quantité suivante.


\begin{definition} 
La corrélation (ou coefficient de corrélation linéaire de Pearson) entre les observations de $x$ et de $y$ est définie par
$$
\rho(x, y)=\frac{\operatorname{Cov}(x, y)}{\sqrt{\operatorname{Var}(x)} \sqrt{\operatorname{Var}(y)}}=\frac{\operatorname{Cov}(x, y)}{\sigma_{x} \sigma_{y}} .
$$
\end{definition} 

Il est évident que le signe de la corrélation s'interprète toujours comme celui de la covariance.
En particulier, il n'y a toujours pas d'interprétation directe lorsque la corrélation est proche de zéro.
Cependant, il est possible d'interpréter la valeur de la corrélation car celle-ci est une quantité bornée
et indépendante de l'échelle dans laquelle sont exprimées les observations de $x$ et de $y$.

\begin{thm}
\label{thm.corr}
La corrélation est bornée par 1 en valeur absolue,
$$
-1 \leq \rho(x, y) \leq 1
$$
\end{thm}

\begin{proof}
Il est possible de voir les observations de $x$ et de $y$ comme deux vecteurs $\left(x_{1}-\bar{x}, \ldots, x_{n}-\bar{x}\right)^{\prime}$ et $\left(y_{1}-\bar{y}, \ldots, y_{n}-\bar{y}\right)^{\prime}$ de $\mathbb{R}^{n}$. 
Ainsi, par la Proposition 3.1, la covariance $\operatorname{Cov}(x, y)$ entre $x$ et $y$ 
est un produit scalaire (i.e. une forme bilinéaire symétrique définie positive, voir le cours d'algèbre
linéaire) entre ces vecteurs. De plus, la norme associée à ce produit scalaire
est $\sqrt{\operatorname{Var}(x)}$. La preuve du théorème est donc une 
conséquence directe de l'Inégalité de Cauchy-Schwarz que nous re-démontrons ici.

Soit $t \in \mathbb{R}$, nous considérons la variable quantitative $z=x+t y$. En utilisant les Propositions $1.2,3.1$ et 3.2 , nous obtenons

$$
\operatorname{Var}(z)=\operatorname{Var}(x+t y)=\operatorname{Var}(x)+2 t \operatorname{Cov}(x, y)+t^{2} \operatorname{Var}(y)
$$

La variance de $z$ est donc un polynôme du second degré en $t$. Comme Var $(z) \geqslant 0$ pour tout $t \in \mathbb{R}$, nous savons que ce polynôme a au plus une racine réelle et que son discriminant $\Delta$ est négatif,

$$
\Delta=4 \operatorname{Cov}(x, y)^{2}-4 \operatorname{Var}(x) \operatorname{Var}(y) \leqslant 0
$$

Autrement dit,

$$
\begin{aligned}
\operatorname{Cov}(x, y)^{2} \leqslant \operatorname{Var}(x) \operatorname{Var}(y) & \Longleftrightarrow|\operatorname{Cov}(x, y)| \leqslant \sqrt{\operatorname{Var}(x)} \sqrt{\operatorname{Var}(y)} \\
& \Longleftrightarrow|\rho(x, y)| \leqslant 1
\end{aligned}
$$

La valeur de $\rho(x, y)$ nous renseigne donc sur l'importance du lien potentiel entre $x$ et $y$. Plus particulièrement, nous avons que plus $|\rho(x, y)|$ est proche de 1 , plus la relation affine entre les variables $x$ et $y$ est avérée comme nous allons le voir dans la section suivante.
\end{proof}


\begin{ex}
Montrer que si les points observés $\left(x_{1}, y_{1}\right), \ldots,\left(x_{n}, y_{n}\right)$ 
sont sur une droite d'équation  $y=a x+b$  alors $|\rho(x, y)|=1$. 
Réciproquement, montrer que si $|\rho(x, y)|=1$ alors les points observés
$\left(x_{1}, y_{1}\right), \ldots,\left(x_{n}, y_{n}\right)$ sont tous alignés le long
d'une droite dont on donnera l'équation selon que $\rho(x, y)=1$ ou que $\rho(x, y)=-1$. 
(Utiliser l'exercice 1.4 et la preuve du Théorème 3.1)
\end{ex}

%---------------    
\subsection{Régression linéaire}
%regression/covariance:  %https://www.math.univ-toulouse.fr/~xgendre/ens/l3sid/StatExplo.pdf
%mathpix  https://www.reddit.com/r/LaTeX/comments/pu3xnh/mathpix_free_equivalent/

%REF https://plmlab.math.cnrs.fr/wikistat

Lorsque nous cherchons à établir une relation entre deux variables quantitatives
 $x$ et $y$, une première approche simple consiste à regarder si il existe une relation affine
(i.e. de la forme $y=a x+b$ avec $a, b \in \mathbb{R}$ ) entre elles. 
 Bien entendu, en pratique, il est presque toujours impossible d'établir une telle relation 
 de façon exacte entre les observations de $x$ et celles de $y$.
  Cependant, nous pouvons chercher la droite qui explique "au mieux" $y$ par rapport à $x$.

Cette procédure s'appelle la régression linéaire et elle se formalise comme ce qui suit. 
Nous cherchons deux nombres réels $a$ et $b$ tels que l'erreur commise en expliquant 
les observations $y_{i}$ par $a x_{i}+b, i \in\{1, \ldots, n\}$, soit la plus petite possible 
au sens des moindres carrés. Autrement dit, nous cherchons $a, b \in \mathbb{R}$ 
tels que l'erreur moyenne:


\begin{equation}
\label{eq.regression-erreur}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-\left(a x_{i}+b\right)\right)^{2} 
\end{equation}


soit minimale (voir Figure \ref{fig:regression-erreur}). 
Les valeurs de $a$ et de $b$ telles que cette erreur soit minimale donnent l'équation $y=a x+b$ de la droite de régression.

\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{PDFSVG/regression_erreur}
\caption{\label{fig:regression-erreur}Erreurs (en rouge) commises entre les observations et la droite $y=0.5-x$. Source: \cite{Gendre12} }
\end{figure}


\begin{thm}
\label{thm.regression.eq}
Si les variables $x$ et $y$ sont centrées et réduites alors l'erreur (\ref{eq.regression-erreur}) 
est minimale pour $a=\operatorname{Cov}(x, y)$ et $b=0$. 
Dans ce cas, l'équation de la droite de régression est donc:
$$
y=\operatorname{Cov}(x, y) \times x
$$
\end{thm}


\begin{proof}
Nous commençons par développer l'erreur (\ref{eq.regression-erreur}) 
en utilisant le fait que $\bar{x}=\bar{y}=0$,

$$
\begin{aligned}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-\left(a x_{i}+b\right)\right)^{2} & =\frac{1}{n} \sum_{i=1}^{n}\left(\left(y_{i}-a x_{i}\right)-b\right)^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}-\frac{2 b}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)+\frac{1}{n} \sum_{i=1}^{n} b^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}-2 b(\bar{y}-a \bar{x})+b^{2} \\
& =\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2}+b^{2}
\end{aligned}
$$

La quantité que nous cherchons à minimiser est la somme de deux termes positifs, 
l'un dépendant de $a$ et l'autre de $b$.
Nous obtenons donc directement que $b=0$. Pour déterminer $a$, nous continuons 
à développer cette quantité en utilisant le fait que
 $\overline{x^{2}}=\operatorname{Var}(x)=1$ et $\overline{y^{2}}=\operatorname{Var}(y)=1$,

$$
\begin{aligned}
\frac{1}{n} \sum_{i=1}^{n}\left(y_{i}-a x_{i}\right)^{2} & =\frac{1}{n} \sum_{i=1}^{n} y_{i}^{2}-\frac{2 a}{n} \sum_{i=1}^{n} y_{i} x_{i}+\frac{a^{2}}{n} \sum_{i=1}^{n} x_{i}^{2} \\
& =\overline{y^{2}}-2 a \overline{x y}+a^{2} \overline{x^{2}} \\
& =a^{2}-2 \overline{x y} \times a+1
\end{aligned}
$$

Nous minimisons donc ce polynôme du second degré en $a=\overline{x y}=\operatorname{Cov}(x, y)$.
\end{proof}

\begin{ex}
Soient des données centrées réduites $\{X_i,Y_i\} i\in[1;N]$. 
\begin{itemize}
\item Vérifier que les données sont centrées-réduites (calculer la moyenne et l'écart-type).
\item Calculer $\operatorname{Cov}(X, Y)$
\item Tracer la droite $y=\operatorname{Cov}(X, Y) \times x$
\end{itemize}


    \begin{tabular}{rrrrrrrrr}  % d'après exo ressort, après centré-réduit
    \toprule
     & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7  \\
    \midrule
    X& -0.75  & -0.33  & 0.79  & 1.07 & -1.04& 1.64 & -0.05 & -1.32 \\
    Y& -0.68 & -0.33 & 0.72 & 1.07 & -1.04 & 1.64  & 0.01 & -1.39  \\
    \bottomrule
    \end{tabular}
    
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{PDFSVG/reg_lin_centre_reduit}
    \caption{Droite de régression $y=Cov(x,y) x$ en pointillés rouge. Nuage de points $(\frac{x-\bar{x}}{\sigma_x},\frac{y-\bar{y}}{\sigma_y} )$ en points bleus. }
    \end{figure}
    
    
    \begin{verbatim}
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    x_cent_scale = (x- x_mean)/np.std(x)
    y_cent_scale = (y- y_mean)/np.std(y)
    cov = np.cov(x_cent_scale,y_cent_scale)[0,1] 
    cov = 1.14
    \end{verbatim}
    
\end{ex}


Le résultat du théorème \ref{thm.regression.eq} se généralise à des variables 
quantitatives $x$ et $y$ qui ne sont plus supposées être centrées réduites de la façon suivante.

\begin{thm}
\label{thm.regression.eq.general}
Dans le cas général, l'erreur (\ref{eq.regression-erreur})  est minimale pour $a=\operatorname{Cov}(x, y) / \operatorname{Var}(x)$ et $b=\bar{y}-a \bar{x}$ et l'équation de la droite de régression est donnée par
$$
y=\frac{\operatorname{Cov}(x, y)}{\operatorname{Var}(x)} x+\left[\bar{y}-\frac{\operatorname{Cov}(x, y)}{\operatorname{Var}(x)} \bar{x}\right]
$$
\end{thm}


\begin{proof}
 Considérons les variables quantitatives $x^{\prime}$ et $y^{\prime}$ obtenues en centrant et en réduisant $x$ et $y$ respectivement,

$$
x^{\prime}=\frac{x-\bar{x}}{\sigma_{x}} \quad \text { et } \quad y^{\prime}=\frac{y-\bar{y}}{\sigma_{y}} .
$$

Pour ces variables, le Théorème \ref{thm.regression.eq.general} nous donne l'équation de la droite de régression,

$$
\begin{aligned}
y^{\prime}=\operatorname{Cov}\left(x^{\prime}, y^{\prime}\right) \times x^{\prime} & \Longleftrightarrow \frac{y-\bar{y}}{\sigma_{y}}=\operatorname{Cov}\left(\frac{x-\bar{x}}{\sigma_{x}}, \frac{y-\bar{y}}{\sigma_{y}}\right) \times \frac{x-\bar{x}}{\sigma_{x}} \\
& \Longleftrightarrow y-\bar{y}=\frac{\operatorname{Cov}(x, y)}{\sigma_{x} \sigma_{y}} \times \frac{\sigma_{y}}{\sigma_{x}} \times(x-\bar{x}) \\
& \Longleftrightarrow y=\underbrace{\frac{\operatorname{Cov}(x, y)}{\sigma_{x}^{2}}}_{=a} x+\underbrace{\bar{y}-\frac{\operatorname{Cov}(x, y)}{\sigma_{x}^{2}} \bar{x}}_{=b}
\end{aligned}
$$

Nous retrouvons dans ce résultat le fait que si $\operatorname{Cov}(x, y)>0$, 
alors les variables varient dans le même sens et inversement (voir Figures \ref{fig.regression.exemple.corr}  et \ref{fig.regression.exemple.uncorr} ).

\end{proof}

\begin{figure}
\includegraphics[width=\textwidth]{PDFSVG/regression_nuage_corr}
\caption{\label{fig.regression.exemple.corr} Tracés des observations $\left(x_{i}, y_{i}\right)$ et de la droite de régression (en rouge) associée. A gauche, $\rho(x, y)=0.9855$ et à droite, $\rho(x, y)=-0.9863$.  Source: \cite{Gendre12} }
\end{figure}



\begin{figure}
\includegraphics[width=\textwidth]{PDFSVG/regression_nuage_uncorr}
\caption{\label{fig.regression.exemple.uncorr} Tracés des observations $\left(x_{i}, y_{i}\right)$ et de la droite de régression (en rouge) associée. Dans les deux cas, $\rho(x, y)=0$ et nous voyons qu'il n'est pas possible d'interpréter cette valeur car les variables $x$ et $y$ peuvent être indépendantes (à gauche) ou liées (à droite). Source: \cite{Gendre12} }
\end{figure}

\begin{proposition}
Lorsque la relation entre $x$ et $y$ n'est pas linéaire, on peut se ramener quand même 
à une regression linéaire en transformant $x$ et/ou $y$.
\end{proposition}

\begin{ex}~\\

    \begin{tabular}{rrrrrrrrr}
    \toprule
     & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7  \\
    \midrule
    x& 5  &6  &11  &18 & 25&31 & 41 & 42 \\
    y& 3 & 3.3 & 6 & 10 & 19 & 27  & 54 & 59  \\
    $\ln(y)$ &1.09  &1.19 & 1.79 & 2.30 & 2.94 & 3.29  &3.98  & 4.07  \\
    \bottomrule
    \end{tabular} 
    
\begin{itemize}
\item Calculer la moyenne et l'écart-type de $x$ et $y$.
\item Centrer-réduire $x$ et $y$.
\item Calculer $\operatorname{Cov}(X, Y)$
\item Tracer la droite $y=\operatorname{Cov}(X, Y) \times x$
\item Recommencer les opérations précédentes en remplaçant $y$ par $\ln(y)$
\end{itemize}
    
\end{ex}

%%---------------    
%\section*{BONUS: quantiles, quartiles, étendue interquartile}


%\begin{definition} 
%Le \textbf{quantile d'ordre} $\alpha$ coupe (approximativement) les données en $\alpha\%$ et $(1-\alpha)\%$. On calcule $ \lceil n \cdot \alpha \rceil $ et on en déduit
%\begin{gather*}  
       %\hat{q}(\alpha) = x_{ (\lceil n\alpha \rceil) }.
%\end{gather*}
%Les \textbf{quartiles} correspondent au cas $\alpha = 0.25$ et $\alpha = 0.75$.  La médiane à $\alpha = 0.5$.
%\end{definition} 

%\begin{definition} 
%\textbf{L'étendue (ou l'écart) interquartile} est définie comme:
%\begin{gather*}
 %\text{EIQ} = \hat{q}(0.75) - \hat{q}(0.25) 
%\end{gather*}
%\end{definition} 

%%---------------    
%\section*{BONUS: IA}

%LIEN AVEC IA (e.g. images) : regressions est une forme plus complexe


%%---------------    
%\section*{BONUS: Représentation graphique boxplot}

%Le \textbf{boxplot} (ou boîte à moustache pour les puristes) est un type de représentation graphique particulier, très apprécié des statisticiens. Il permet:
%\begin{itemize}
%\item la visualisation de l'asymétrie;
%\item la comparaison facile de jeux de données;
%\item la visualisation de valeurs aberrantes.
%\end{itemize}

%Les cinq valeurs suivantes donnent un bonne description d'une variable: 
%\begin{equation*}
    %\text{minimum, $\hat{q}(25\%)$, médiane, $\hat{q}(75\%)$, maximum}.
%\end{equation*} 
 
%Cette liste est à la base de la \textbf{boîte à moustaches}, Fig.\ref{fig:boxplot}.

%\begin{figure}
%\centering
%\includegraphics[width=0.3\textwidth]{PDFSVG/Boxplot_weight}
%\caption{\label{fig:boxplot} Boxplot du poids des étudiants américains. }
%\end{figure}


%Les bords du rectangle sont donnés par les quartiles. On indique la médiane par une ligne qui sépare la boîte.
%Pour les moustaches, on calcule 1.5 fois l'étendue interquartile:
%\begin{gather*} C = 1.5 \cdot \text{EIQ} = 1.5 \cdot \big[ \hat{q}(75\%) - \hat{q}(25\%) \big] \end{gather*}
%Puis on calcule $ \hat{q}(25\%) - C $ et $ \hat{q}(75\%) + C $.
%Typiquement, toutes les valeurs de la variables sont entre ces seuils.
%\begin{itemize}
%\item \textbf{Si oui}, on met les limites des moustaches au \textbf{minimum} et \textbf{maximum}.
%\item Sinon, on mets les moustaches aux valeurs observées les plus proches des seuils \textbf{contenues dans les moustaches} et  on représente \textbf{les valeurs exceptionnelles par des points}.
%\end{itemize}
%Cela donne, en pratique, la Fig.\ref{fig:boxplot_example}.

%\begin{figure}
%\centering
%\includegraphics[width=0.3\textwidth]{PDFSVG/Boxplot_example}
%\caption{\label{fig:boxplot_example} Exemple de boxplot. M: Médiane ; $\text{Q}_{.25}$: quantile à 25\%; EIQ:  écart inter-quartile; $\text{Q}_{.25}$: quantile à 75\%}
%\end{figure}

\appendix
\newpage
%---------------    
\section*{ANNEXE: propriétés de la covariance}

%Proposition 3.1. 
\begin{proposition}
Soient a un nombre réel quelconque et $z_{1}, \ldots, z_{n} \in \mathbb{R}$ les observations d'une variable quantitative $z$, la covariance vérifie les propriétés suivantes :

\begin{itemize}
  \item Bilinéarité :
\end{itemize}

$$
\begin{gathered}
\operatorname{Cov}(a x, y)=a \operatorname{Cov}(x, y) \quad \text { et } \quad \operatorname{Cov}(x, a y)=a \operatorname{Cov}(x, y) \\
\operatorname{Cov}(x, y+z)=\operatorname{Cov}(x, y)+\operatorname{Cov}(x, z)
\end{gathered}
$$

et

$$
\operatorname{Cov}(x+z, y)=\operatorname{Cov}(x, y)+\operatorname{Cov}(z, y)
$$

\begin{itemize}
  \item Symétrie :
\end{itemize}

$$
\operatorname{Cov}(x, y)=\operatorname{Cov}(y, x)
$$

\begin{itemize}
  \item Positivité :
\end{itemize}

$$
\operatorname{Cov}(x, x) \geqslant 0
$$
\end{proposition}

\begin{proof}
La covariance est symétrique par définition,

$$
\operatorname{Cov}(x, y)=\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)=\sum_{i=1}^{n} p_{i}\left(y_{i}-\bar{y}\right)\left(x_{i}-\bar{x}\right)=\operatorname{Cov}(y, x)
$$

Pour avoir sa bilinéarité, par symétrie, il suffit de montrer la linéarité en $x$. La Proposition 1.1 donne

$$
\operatorname{Cov}(a x, y)=\sum_{i=1}^{n} p_{i}\left(a x_{i}-\overline{a x}\right)\left(y_{i}-\bar{y}\right)=a \sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)=a \operatorname{Cov}(x, y)
$$

et

$$
\begin{aligned}
\operatorname{Cov}(x, y+z) & =\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(\left(y_{i}-\bar{y}\right)+\left(z_{i}-\bar{z}\right)\right) \\
& =\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)+\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(z_{i}-\bar{z}\right) \\
& =\operatorname{Cov}(x, y)+\operatorname{Cov}(x, z)
\end{aligned}
$$

Remarquons enfin que la covariance d'une variable avec elle-même est sa variance,

$$
\operatorname{Cov}(x, x)=\operatorname{Var}(x) \geqslant 0
$$

La variance n'est pas additive. Cependant, il est possible de développer la variance 
d'une somme de variables quantitatives en faisant intervenir la covariance.
\end{proof}

\begin{proposition}
Nous avons:

$$
\operatorname{Var}(x+y)=\operatorname{Var}(x)+2 \operatorname{Cov}(x, y)+\operatorname{Var}(y)
$$
\end{proposition}

\begin{proof}
Il suffit de développer la somme qui définit la variance de $x+y$,

$$
\begin{aligned}
\operatorname{Var}(x+y) & =\sum_{i=1}^{n} p_{i}\left(\left(x_{i}-\bar{x}\right)+\left(y_{i}-\bar{y}\right)\right)^{2} \\
& =\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)^{2}+2 \sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)+\sum_{i=1}^{n} p_{i}\left(y_{i}-\bar{y}\right)^{2} \\
& =\operatorname{Var}(x)+2 \operatorname{Cov}(x, y)+\operatorname{Var}(y)
\end{aligned}
$$

Pour calculer la covariance $\operatorname{Cov}(x, y)$, il est souvent pratique d'utiliser le résultat suivant.
\end{proof}

\begin{proposition}
La covariance vaut la moyenne des produits moins le produit des moyennes,

$$
\operatorname{Cov}(x, y)=\overline{x y}-\bar{x} \times \bar{y}
$$

avec $\overline{x y}=\sum_{i=1}^{n} p_{i} x_{i} y_{i}$.
\end{proposition}

\begin{proof}
Décomposons la somme de la définition de la covariance,

$$
\begin{aligned}
\operatorname{Cov}(x, y) & =\sum_{i=1}^{n} p_{i}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right) \\
& =\sum_{i=1}^{n} p_{i} x_{i} y_{i}-\bar{x} \sum_{i=1}^{n} p_{i} y_{i}-\bar{y} \sum_{i=1}^{n} p_{i} x_{i}+\bar{x} \times \bar{y} \sum_{i=1}^{n} p_{i} \\
& =\overline{x y}-2 \bar{x} \times \bar{y}+\bar{x} \times \bar{y} \\
& =\overline{x y}-\bar{x} \times \bar{y}
\end{aligned}
$$

\end{proof}




\begin{thebibliography}{99}

\bibitem{PPN2022} MESRI, Annexe 8 Licence professionnelle Bachelor Universitaire de Technologie GÉNIE ÉLECTRIQUE ET INFORMATIQUE INDUSTRIELLE, \href{http://www.enseignementsup-recherche.gouv.fr}{www}, 2022.

\bibitem{Whilhelm22} Whilhelm,M. et al. ``Probabilités et Statistique'', \href{https://matthwilhelm.github.io/ProbaStat/intro.html}{www}, 2022.

\bibitem{Gendre12} Gendre,X., ``Introduction à la Statistique Exploratoire'', Université PaulSabatier, 2012.

\bibitem{Guegnard18} Guegnard, Bourcerie, ``Mathématiques IUT GEII 2e annéeCours, applications, exercices et problèmes corrigés'', Ellipses, 2018.

\end{thebibliography}

\end{document}
