import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def make_cov_tabs(x,y,print=True):
    n = x.shape[0]
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    x_std = np.std(x)
    y_std = np.std(y)
    x_cent_scale = (x-x_mean)/x_std
    y_cent_scale = (y-y_mean)/y_std
    cov = np.cov([x_cent_scale,y_cent_scale])[0,1]
    df=pd.DataFrame({'$x$':x,
                    '$y$':y,
                    '$\hat{x}$':x_cent_scale,
                    '$\hat{y}$':y_cent_scale,
                    '$f_i(x-Ex)(y-Ey)$': 1/n*(x-x_mean)*(y-y_mean),
                    "$f_i \hat{x} \hat{y}$": 1/n * x_cent_scale*y_cent_scale ,
                    })
    if print: print(df.to_latex())
    df_agg = pd.DataFrame({'x':[x_mean,x_std , np.cov([x,y],bias=True)[0,1], np.corrcoef([x,y])[0,1]],
                        'y':[y_mean,y_std , np.cov([x,y],bias=True)[0,1], np.corrcoef([x,y])[0,1]]
                        }
                        , index = ['$E[z]$', '$\sigma_z$','$cov(x,y)$' , '$corr(x,y)$' ] )
    if print: print(df_agg.to_latex())
    return df,df_agg
    
def make_corr_tabs(x,y,print=True):
    n = x.shape[0]
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    x_std = np.std(x)
    y_std = np.std(y)
    x_cent_scale = (x-x_mean)/x_std
    y_cent_scale = (y-y_mean)/y_std
    cov = np.cov([x_cent_scale,y_cent_scale])[0,1]
    df=pd.DataFrame({'$x$':x,
                    '$y$':y,
                    '$\hat{x}$':x_cent_scale,
                    '$\hat{y}$':y_cent_scale,
                    "$f_i \hat{x} \hat{y}$": 1/n * x_cent_scale*y_cent_scale ,
                    })
    if print: print(df.to_latex())
    df_agg = pd.DataFrame({'x':[x_mean,x_std , np.cov([x,y],bias=True)[0,1], np.corrcoef([x,y])[0,1]],
                        'y':[y_mean,y_std , np.cov([x,y],bias=True)[0,1], np.corrcoef([x,y])[0,1]]
                        }
                        , index = ['$E[z]$', '$\sigma_z$','$cov(x,y)$' , '$corr(x,y)$' ] )
    if print:print(df_agg.to_latex())   
    return df,df_agg
    
    
def plot_reg_lin(x,y):
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    print(x_mean)
    print(y_mean)
    x_std = np.std(x)
    y_std = np.std(y)
    x_cent_scale = (x-x_mean)/x_std
    y_cent_scale = (y-y_mean)/y_std
    cov = np.cov([x_cent_scale,y_cent_scale])[0,1]
    #
    plt.scatter(x_cent_scale ,y_cent_scale)
    x_reg = np.array([np.min(x_cent_scale),np.max(x_cent_scale)])
    y_reg = cov * x_reg 
    plt.plot( x_reg,y_reg,'r--')
    plt.show()    
# =============
# CM
# =============

def histogram_tension():
    x = 10 + 0.1*np.random.randn(100)
    fig,ax = plt.subplots(1,1,figsize=(12,5))
    ax.hist(x,10)
    ax.set_xlabel('Tension (V) ')
    ax.set_ylabel("Nombre d'observations")
    fig.savefig("PDFSVG/histogramme_tension.pdf")
    
def  tab_tension():   
    n= 10
    x = 10 + 0.1*np.random.randn(n)
    df = pd.DataFrame( {"Tension(V)": np.concatenate([x, 
                                                                                    np.array([np.mean(x)])]) })
    #arrays = [["Mesure"]+[""]*n+['Moyenne'], ['red', 'blue', 'red', 'blue']]
    array = ["Mesure"]+[""]*(n-1)+['Moyenne']
    df.index = array
    #df.index = pd.MultiIndex.from_arrays(arrays, names=('Données', 'Moyenne'))
    df.to_latex(buf = "PDFSVG/tab_tension.tex", index=True,
                     float_format="%.3f", label="tab.tension", decimal=",",
                    caption="Tension(V). Mesures et calcul de la moyenne.")

def  tab_tension_mediane():   
    n= 11
    x = 10 + 0.1*np.random.randn(n)
    df = pd.DataFrame( {"Tension(V)": np.concatenate([x, 
                                                                                    np.array([np.median(x)])]) })
    df['Tension(V) par ordre croissant'] = np.concatenate([np.sort(x), 
                                                                                    np.array([np.median(x)])]) 
    #arrays = [["Mesure"]+[""]*n+['Moyenne'], ['red', 'blue', 'red', 'blue']]
    array = ["Mesure"]+[""]*(n-1)+['Mediane']
    df.index = array
    #df.index = pd.MultiIndex.from_arrays(arrays, names=('Données', 'Moyenne'))
    df.to_latex(buf = "PDFSVG/tab_tension_mediane.tex", index=True,
                     float_format="%.3f", label="tab.tension.mediane", decimal=",",
                    caption="Tension(V). Mesures, mesures ordonnées, calcul de la moyenne.")

def ex_reg_lin_centre_reduit():
    """
    """
    x = np.array([ 7  ,10  ,18  , 20 , 5 , 24 , 12 , 3])
    x_mean = np.mean(x)
    x_std = np.std(x)
    x_cent_scale = (x- x_mean)/x_std    
    y = np.array([8.5, 9 ,10.5 ,11 , 8 , 11.8  , 9.5 , 7.5 ])
    y_mean = np.mean(y)
    y_std = np.std(y)
    y_cent_scale = (y- y_mean)/y_std
    cov = np.cov(x_cent_scale,y_cent_scale)[0,1] #off-diagonal
    plt.scatter(x_cent_scale,y_cent_scale)
    x_reg = np.array([np.min(x_cent_scale),np.max(x_cent_scale)])
    y_reg = x_reg * cov
    plt.plot( x_reg,y_reg,'r--')
    plt.show()

# =============
# TD
# =============

def  tab_tension_mediane_TD():   
    n= 7
    x = 1 + 0.1*np.random.randn(n)
    df = pd.DataFrame( {"Tension(V)": np.concatenate([x, 
                                                                                    np.array([np.median(x)])]) })
    df['Tension(V) par ordre croissant'] = np.concatenate([np.sort(x), 
                                                                                    np.array([np.median(x)])]) 
    #arrays = [["Mesure"]+[""]*n+['Moyenne'], ['red', 'blue', 'red', 'blue']]
    array = ["Mesure"]+[""]*(n-1)+['Mediane']
    df.index = array
    #df.index = pd.MultiIndex.from_arrays(arrays, names=('Données', 'Moyenne'))
    df.to_latex(buf = "PDFSVG/tab_tension_mediane_TD.tex", index=True,
                     float_format="%.3f", label="tab.tension.mediane.TD", decimal=",",
                    caption="Tension(V). Mesures, mesures ordonnées, calcul de la moyenne.")

def TD2_exo_interquartile():
    np.random.seed(42)
    x1 = np.sort(2*np.random.randn(30) +2)
    x2 = np.sort(np.random.randn(30) +10)
    x3=  np.sort(2*np.random.randn(30) +2)
    x4 = np.sort(np.random.randn(30) +10)
    df = pd.DataFrame( {1: x1, 2:x2,3:x3, 4:x4})
    df.to_latex(buf = "PDFSVG/tab_interquartile_TD.tex", index=True,
                     float_format="%.3f", label="tab.interquartile.TD", decimal=",",
                    caption="Mesures expérimentales ordonnées par ordre croissant.")

def TD3_exo1_covar():
    """
    """
    # 
    np.random.seed(42)
    n = 10
    x = np.arange(1,n+1)
    x = x + np.random.rand(n)
    y = 0.5*x + np.random.randn(n)
    #plt.scatter(x,y); plt.show()
    s=""
    for (x_,y_) in zip(x,y): s+="({:2.2},{:2.2})".format(x_,y_) 
    #df = pd.DataFrame( {'x':x, 'y':y})
    #df.T.to_latex( float_format="%.3f", decimal="," , index= False)
    # 
    n = 10
    x = np.arange(1,n+1)
    x = x + 2* np.random.rand(n)
    y = 0.5*x + 0.1 *np.random.randn(n)
    plt.scatter(x,y); plt.show()
    s=""
    for (x_,y_) in zip(x,y): s+="({:2.2},{:2.2})".format(x_,y_) 
    #df = pd.DataFrame( {'x':x, 'y':y})
    #df.T.to_latex( float_format="%.3f", decimal="," , index= False)

def TD3_exo2_covar():
    """
    """
    np.random.seed(42)
    # 1  cor
    n = 10
    x = np.arange(1,n+1)
    x = x + np.random.rand(n)
    y = 0.5*x + np.random.randn(n)
    plt.scatter(x,y); plt.show()
    #s=""
    #for (x_,y_) in zip(x,y): s+="({:2.2},{:2.2})".format(x_,y_) 
    df = pd.DataFrame( {'x':x, 'y':y})
    df.T.to_latex( float_format="%.3f", decimal="," , index= False)
    # 2 decor
    x = 10 * np.random.rand(n)
    y = 5 * np.random.rand(n)
    df = pd.DataFrame( {'x':x, 'y':y})
    df.T.to_latex( float_format="%.3f", decimal="," , index= False)    
    # 3  cor # TODO: ANTICOR ??
    n = 10
    x = np.arange(1,n+1)
    x = x + 2* np.random.rand(n)
    y = 0.5*x + 0.1 *np.random.randn(n)
    plt.scatter(x,y); plt.show()
    #s=""
    #for (x_,y_) in zip(x,y): s+="({:2.2},{:2.2})".format(x_,y_) 
    df = pd.DataFrame( {'x':x, 'y':y})
    df.T.to_latex( float_format="%.3f", decimal="," , index= False)
    # 4 decor
    x = 10 * np.random.rand(n)
    y = 5 * np.random.rand(n)
    df = pd.DataFrame( {'x':x, 'y':y})
    df.T.to_latex( float_format="%.3f", decimal="," , index= False)

def TD4_exo3_reg_lin():
    x = np.array([ 6,11,17,19,6,25,10,4 ])
    y=0.22 *x + 6.65
     
def TD4_exo3_reg_nonlin():
    #np.random.seed(42)
    #x = np.random.choice(range(4,43),8, replace=False)
    x = np.array( [4 , 6,  12,  17,  20,31 , 39, 42])
    y = 2.256 * 1.082**x
    plt.scatter(x,y); plt.show()
    df = pd.DataFrame( {'x':x, 'y':y})
    df.T.to_latex( float_format="%.3f", decimal="," , index= False)

# ===================
# DSB

def DSB_exo_4():
    np.random.seed(43)
    # 1  anticor
    n = 10
    x = np.arange(1,n+1)
    x = x + np.random.rand(n)
    y = 5- 0.5*x + np.random.randn(n)
    df1 = pd.DataFrame( {'x':x, 'y':y})
    print(df1.T.to_latex( float_format="%.3f", decimal="," , index= False))
    plt.scatter(x,y); plt.show()
    #print(df)
    # 2 decor
    x = 10 * np.random.rand(n)
    y = 5 * np.random.rand(n)
    df2 = pd.DataFrame( {'x':x, 'y':y})
    #df.T.to_latex( float_format="%.3f", decimal="," , index= False)    
    #print(df)
    print(df2.T.to_latex( float_format="%.3f", decimal="," , index= False))
    plt.scatter(x,y); plt.show()
    return df1,df2

def DSB_exo_5_reg_nonlin():
    #np.random.seed(42)
    #x = np.random.choice(range(4,43),8, replace=False)
    x = np.array( [3.5 , 5.5,  12.5,  16,  19,30 , 38, 40.4])
    y = 2.256 * 1.082**x
    df = pd.DataFrame( {'x':x, 'y':y})
    print(df.T.to_latex( float_format="%.3f", decimal="," , index= False))
    plt.scatter(x,y); plt.show()
    return df

if __name__ == '__main__':
    #np.random.seed(42)
    histogram_tension()
    tab_tension()
    tab_tension_mediane()
